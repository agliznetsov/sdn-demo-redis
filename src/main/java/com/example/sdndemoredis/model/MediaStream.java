package com.example.sdndemoredis.model;


import lombok.Data;

@Data
public class MediaStream {
	String data;

	String source;

	String destination;

	long version;

	long time;
}
