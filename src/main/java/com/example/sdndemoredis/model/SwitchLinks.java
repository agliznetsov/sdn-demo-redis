package com.example.sdndemoredis.model;



import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class SwitchLinks {
	Map<String, Integer> bandwidth = new HashMap<>();

	long version;
}
