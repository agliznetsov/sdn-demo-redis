package com.example.sdndemoredis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.sdndemoredis.model.MediaStream;
import com.example.sdndemoredis.model.SwitchLinks;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

@Component
public class MediaStreamService {
	public static final String SEPARATOR = "/";
	private static final String MEDIASTREAM_TYPE = "MediaStream";
	private static final String KEY_MEDIASTREAM = MEDIASTREAM_TYPE + SEPARATOR + "%s";
	private static final String KEY_LINK = "SwitchLinks";

	@Autowired
	JedisPool jedisPool;

	ObjectMapper objectMapper = new ObjectMapper();


	String data;

	public MediaStreamService() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 1024; i++) {
			sb.append(' ');
		}
		data = sb.toString();
	}

	@SneakyThrows
	public void createData() {
		try (Jedis jedis = jedisPool.getResource()) {
			for (int i = 0; i < 1000; i++) {
				MediaStream ms = new MediaStream();
				ms.setSource(String.valueOf(i));
				ms.setDestination(String.valueOf(i + 1));
				ms.setData(data);

				String key = String.format(KEY_MEDIASTREAM, ms.getSource());
				jedis.set(key, encode(ms));
			}

			SwitchLinks link = new SwitchLinks();
			jedis.set(KEY_LINK, encode(link));
		}
	}

	public MediaStream getStream(String id) {
		try (Jedis jedis = jedisPool.getResource()) {
			String key = String.format(KEY_MEDIASTREAM,id);
			return decode(jedis.get(key), MediaStream.class);
		}
	}

	@SneakyThrows
	public boolean updateStream(String source, String destination, List<String> links) {
		List<String> keys = new ArrayList<>();
		keys.add(String.format(KEY_MEDIASTREAM, source));
		keys.add(String.format(KEY_MEDIASTREAM, destination));

		try (Jedis jedis = jedisPool.getResource()) {
			jedis.watch(keys.get(0), keys.get(1), KEY_LINK);

			List<String> values = jedis.mget(keys.toArray(new String[0]));
			List<MediaStream> streams = new ArrayList<>();
			values.forEach(v -> {
				MediaStream ms = decode(v, MediaStream.class);
				ms.setTime(System.currentTimeMillis());
				ms.setVersion(ms.getVersion() + 1);
				streams.add(ms);
			});

			SwitchLinks switchLinks = decode(jedis.get(KEY_LINK), SwitchLinks.class);
			links.forEach(k -> switchLinks.getBandwidth().put(k, 0));

			Transaction tx = jedis.multi();
			for (int i = 0; i < keys.size(); i++) {
				tx.set(keys.get(i), encode(streams.get(i)));
			}
			tx.set(KEY_LINK, encode(switchLinks));
			Object response = tx.exec();
			if (response == null) {
				return false;
			}
			return true;
		}
	}

	@SneakyThrows(IOException.class)
	protected <T> T decode(String in, Class<T> targetClass) {
		return in == null ? null : objectMapper.readValue(in, targetClass);
	}

	@SneakyThrows(IOException.class)
	protected <T> String encode(T in) {
		return in == null ? null : objectMapper.writeValueAsString(in);
	}
}
