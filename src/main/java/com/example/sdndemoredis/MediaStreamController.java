package com.example.sdndemoredis;


import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MediaStreamController {
	@Autowired
	MediaStreamService mediaStreamService;

	@PostMapping("/data")
	public void createData() {
		mediaStreamService.createData();
	}

	@PostMapping("/stream")
	public ResponseEntity<String> updateStream(@RequestParam("source") String source, @RequestParam("destination") String destination,
	                                   @RequestParam("links") String links) {
		if (!mediaStreamService.updateStream(source, destination, Arrays.asList(links.split(",")))) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("");
		}
		return ResponseEntity.ok("");
	}
}
