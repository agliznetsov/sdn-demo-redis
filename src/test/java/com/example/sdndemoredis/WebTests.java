package com.example.sdndemoredis;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class WebTests {
	static List<RestTemplate> templates = new ArrayList<>();

	Random random = new Random();
	AtomicInteger errorCounter = new AtomicInteger();
	AtomicInteger retryCounter = new AtomicInteger();


	@BeforeAll
	static void beforeAll() {
		for (int i = 1; i <= 1; i++) {
			String url = "http://localhost:808" + i;
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(url));
			templates.add(restTemplate);
		}
	}

	@Test
	void createData() {
		templates.get(0).postForEntity("/data", null, String.class);
	}

	@Test
	void updateMulti() {
		ExecutorService executor = Executors.newCachedThreadPool();
		int threads = 1;
		while (threads <= 64) {
			errorCounter.set(0);
			retryCounter.set(0);

			List<Future<Integer>> futures = new ArrayList<>();
			for(int i=0; i<threads; i++) {
				int id = i;
				futures.add(executor.submit(() -> clientThread(id)));
			}
			long start = System.currentTimeMillis();
			int sum = futures.stream().mapToInt(this::resolve).sum();
			long time = System.currentTimeMillis() - start;

			log.info("Threads: {} Run time: {} Requests: {} Errors: {} Retries: {} Req per sec: {}",
					threads, time, sum, errorCounter.get(), retryCounter.get(),
					(sum * 1000 / time));
			threads = threads * 2;
		}
	}

	@SneakyThrows
	private int resolve(Future<Integer> integerFuture) {
		return integerFuture.get();
	}


	private int clientThread(int id) {
		//		log.info("Thread {} started", id);
		int counter = 0;
		long start = System.currentTimeMillis();
		while (System.currentTimeMillis() - start < 10000) {
			String source = String.valueOf(random.nextInt(1000));
			String destination = String.valueOf(random.nextInt(1000));
			List<String> links = new ArrayList<>();
			for (int i = 0; i < 5; i++) {
				links.add(String.valueOf(random.nextInt(100)));
			}
			if (updateStream(source, destination, links)) {
				counter++;
			} else {
				errorCounter.incrementAndGet();
			}
		}
		//		log.info("Thread {} finished", id);
		return counter;
	}

	@SneakyThrows
	private boolean updateStream(String source, String destination, List<String> links) {
		for (int i = 0; i < 5; i++) {
			try {
				doUpdateStream(source, destination, links);
				return true;
			} catch (Exception e) {
				retryCounter.incrementAndGet();
				Thread.sleep(random.nextInt(10));
				//log.error(e.getMessage());
			}
		}
		return false;
	}

	private void doUpdateStream(String source, String destination, List<String> links) {
		int id = random.nextInt(templates.size());
		RestTemplate template = templates.get(id);
		String linkString = String.join(",", links);
		String url = "/stream?source=" + source + "&destination=" + destination + "&links=" + linkString;
		template.postForEntity(url, null, String.class);
	}
}
